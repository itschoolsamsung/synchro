package com.example.synchro;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

public class MainActivity extends Activity implements View.OnClickListener {

    TextView text;
    Sum sum;

    private void updateText(final String value) { //метод-обёртка для обращения к виджету из другого потока
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                text.setText(value);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = findViewById(R.id.textView);
        sum = new Sum();
        sum.setSum(100);

        findViewById(R.id.button).setOnClickListener(this);
        findViewById(R.id.button2).setOnClickListener(this);
        findViewById(R.id.button3).setOnClickListener(this);
        findViewById(R.id.button4).setOnClickListener(this);
        findViewById(R.id.button5).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button: //запускаем первый поток с циклом и задержками
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        for (int i=0; i<10; i++) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                            }
                            Log.i("thread-1", String.valueOf(i));
                            updateText("thread-1 " + String.valueOf(i));
                        }
                    }
                };
                new Thread(r).start();
                break;
            case R.id.button2: //запускаем второй поток с циклом и задержками
                Runnable r2 = new Runnable() {
                    @Override
                    public void run() {
                        for (int i=0; i<10; i++) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                            }
                            Log.i("thread-2", String.valueOf(i));
                            updateText("thread-2 " + String.valueOf(i));
                        }
                    }
                };
                new Thread(r2).start();
                break;
            case R.id.button3: //снимаем с банковского счёта 5 раз в цикле по 10 единиц
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i=0; i<5; i++) {
                            sum.pay(10);
                        }
                    }
                }).start();
                break;
            case R.id.button4: //снимаем с банковского счёта 5 раз в цикле по 15 единиц
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i=0; i<5; i++) {
                            sum.pay(15);
                        }
                    }
                }).start();
                break;
            case R.id.button5: //пополняем банковский счёт до 100 единиц
                sum.setSum(100);
                break;
        }
    }

    /*
    * класс содержит информацию о банковском счёте и методы работы с этим счётом
     */
    class Sum {
        private int sum; //сумма на счету

        public void setSum(int sum) {
            this.sum = sum;
        }

        public void pay(int sum) {
            /*
            * синхронизированный блок кода - будет выполнен только одним потоком
            * второй поток будет ждать, когда первый завершит работу с блоком
             */
            synchronized (this) {
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                }
                Log.i("sum", String.valueOf(this.sum) + "-" + String.valueOf(sum));
                if (this.sum - sum >= 0) { //если на счету > 0, то вычитаем со счёта указанную сумму
                    this.sum -= sum;
                    Log.i("sum", "=" + String.valueOf(this.sum));
                } else { //иначе сообщение об ошибке
                    Log.i("sum", "error");
                }
            }
        }
    }
}
